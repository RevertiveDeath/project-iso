#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <regex>
//Stores compiled data

struct Action
{
	std::string command; //Keyword
	std::vector<int> ints;
	std::vector<float> floats;
	std::vector<std::string> strings;
};

struct Event
{
	int priority;
	int conditionCount;
	int actionCount;
	std::vector<Action> actions;


	Event(){};
};

class Script
{
public:
	enum State
	{
		FAILED, //-1
		OFF, //0
		START, //1
		EVENT, //2
		CONDITION, //3
		ACTION, //4 -> back to 2 if more events
		CHAIN, //5 
		END //X <- after all events and chained files (not needed if eof reached)
	};
public:
	State state;
	std::vector<Event> events;
	int eventCount;
public:
	Script()
	{
		clear();
	}

	void clear()
	{
		state = OFF;
	}

	void initEvents(int count)
	{
		eventCount = count;
		events.clear();
		for (int i = 0; i < eventCount; i++)
			events.push_back(Event());
	}

	void sort()
	{
		//Sorts events by priority
		std::vector<Event> temp = events;
		events.clear();
		int highest;
		int loc;
		for (int i = 0; i < eventCount; i++)
		{
			loc = 0;
			for (unsigned int j = 0; j < temp.size(); j++)
			{
				if (j == 0)
					highest = temp[j].priority;
				else
				{
					if (temp[j].priority > highest)
					{
						highest = temp[j].priority;
						loc = j;
					}
				}
			}
			//Event at loc is next to put in vector
			events.push_back(temp[loc]);
			//Remove from temp
			temp.erase(temp.begin() + loc);
		}
	}

	void update()
	{
		if (events.size() == 0)
			return;

		//Since events are ordered by highest priority first, no sorting in update code
		//Events with equal priority will be run together (and in order the file is written)
		//Events with priority lower than previous events will not be run until previous events are finished
		int priority = events[0].priority; //Priority threshold
		
		std::cout << "LISTING CURRENT ACTIONS: " << std::endl;

		for (unsigned int i = 0; i < events.size(); i++)
		{
			if (events[i].priority < priority)
				break;
			//Check conditions
			//TODO

			//All conditions are met, now we execute the actions in order
			if (events.at(i).conditionCount == 0)
			{
				for (unsigned int j = 0; j < events.at(i).actions.size(); j++)
				{
					//Execute action(j)
					std::cout << "  Action: " << events.at(i).actions.at(j).command << std::endl;
					std::cout << "    Priority: " << events.at(i).priority << std::endl;
					std::cout << "    Parameters: ";
					for each (int k in events.at(i).actions.at(j).ints)
					{
						std::cout << k << " ";
					}
					std::cout << std::endl;			
				}
			}
		}
	}
};