#include "Script.h"

//Datatypes to be extracted from token strings
enum DATATYPE
{
	S_INT,
	S_FLOAT,
	S_STRING,
	S_CONDITION,
	S_ACTION
};

//Used to store temporary param data while extracting from a string
struct Extractor
{
	std::vector<int> ints;
	std::vector<float> floats;
	std::vector<std::string> strings;
	std::vector<std::string> keyword; //Holds conditions/actions

	void clear()
	{
		ints.clear();
		floats.clear();
		strings.clear();
		keyword.clear();
	}
};

static std::string trim(std::string &string)
{
	std::string ret_string;
	//Trims whitespace at end of string
	for (unsigned int i = 0; i < string.size(); i++)
	{
		if (!(string[i] == ' ' || string[i] == '\n' || string[i] == '\t' || string[i] == '\r'))
		{
			ret_string.push_back(string.at(i));
		}
	}

	return ret_string;
}

//Checks if string is a number
static bool isnum(std::string num)
{
	std::string trimmed = (num);
	if (trimmed.size() == 0)
		return false;

	if (trimmed.at(0) != '-' && trimmed.at(0) != '.' && !isdigit(trimmed.at(0)))
		return false;

	bool negative = false;
	bool decimal = false;

	if (trimmed.at(0) == '-')
		negative = true;
	else if (trimmed.at(0) == '.')
		decimal = true;

	for (unsigned int i = 1; i < trimmed.size(); i++)
	{
		if (trimmed.at(i) == '.')
		{
			if (decimal)
				return false;
			else
				decimal = true;
		}
		else if (!isdigit(trimmed.at(i)))
			return false;
	}

	return true;
}

//Used within tokenize. Extracts the specified datatype from the token vector
//Extractor is a reference to prevent duplicate extractor objects performing similar tasks
static void ExtractValues(const std::vector<std::string> &tokens, Extractor &e, DATATYPE d)
{
	e.clear();
	switch (d)
	{
		case(S_INT) :
		{
			for (unsigned int i = 0; i < tokens.size(); i++)
			{
				//If first digit is a digit, the rest must be
				if (isnum(tokens.at(i)))
					e.ints.push_back(std::stoi(tokens.at(i).c_str()));
			}
			break;
		}
		case(S_FLOAT) :
		{
			for (unsigned int i = 0; i < tokens.size(); i++)
			{
				//If first digit is a digit, the rest must be (float version)
				if (isnum(tokens.at(i)))
					e.floats.push_back(std::stof(tokens.at(i).c_str()));
			}
			break;
		}
		case(S_STRING) :
		{
			for (unsigned int i = 0; i < tokens.size(); i++)
			{
				//All strings must precede and trail with quotes, that's it
				if (tokens[i].at(0) == '"' && tokens[i].at(tokens[i].size() - 1) == '"')
					e.strings.push_back(tokens.at(i));
			}
			break;
		}
		//An action case will grab the desired action and its parameters
		case(S_ACTION) :
		{
			for (unsigned int i = 0; i < tokens.size(); i++)
			{
				if (tokens[i] == "ACTION")
				{
					//We know that the next token is our action
					//Any subsequent tokens are parameters
					e.keyword.push_back(tokens[i + 1]);

					for (unsigned int j = i + 2; j < tokens.size(); j++)
					{
						if (tokens[j].find('.') != std::string::npos)
						{
							//float
							e.floats.push_back(std::stof(tokens.at(j).c_str()));
						}
						else if (isnum(tokens.at(j)))
						{
							//int
							e.ints.push_back(std::stoi(tokens.at(j).c_str()));
						}
						else
						{
							//String
							e.strings.push_back(tokens.at(j));
						}
					}
				}
			}
			break;
		}
	}
}

//Tokenize takes the raw script line data and converts it into usable commands and parameters
static std::vector<std::string> Tokenize(Script &s, std::vector<std::string> input)
{
	//Array of tokenized vectors
	std::vector<std::string> chained;

	int size = input.size();
	if (size == 0)
	{
		s.state = Script::FAILED;
		return chained;
	}
	std::vector<std::vector<std::string>> tokenized;
	std::vector<std::string> temp;
	for (int i = 0; i < size; i++)
	{
		temp.clear();
		std::stringstream ss(input.at(i));
		std::string item;
		while (std::getline(ss, item, ' '))
		{
			//Removes any whitespace whatsoever
			item = std::regex_replace(item, std::regex("^ +| +$|( ) +"), "$1");
			if (item.size() > 0)
				temp.push_back(item);
		}
		if (temp.size() > 0)
			tokenized.push_back(temp);
	}

	//Reset size to new vector size (removes all white lines)
	size = tokenized.size();

	//We now have all the tokens
	s.clear();
	s.state = Script::START; //The token we want now is the START token

	Extractor e;
	int events, conditions, actions;
	bool handled;

	for (int a = 0; a < size; a++)
	{
		for each (std::string token in tokenized.at(a))
		{
			handled = false;
			switch (s.state)
			{
			case(Script::START) :
			{
				if (token == "START")
				{
					ExtractValues(tokenized.at(a), e, S_INT);
					//This should extract the number of events, if not, failure
					if (e.ints.size() == 1)
					{
						//Store event count
						events = 0;
						s.initEvents(e.ints[0]);
						if (s.eventCount > 0)
							s.state = Script::EVENT;
						else
							s.state = Script::END;
					}
					else
						s.state = Script::FAILED;
					handled = true;
				}
				break;
			}
			case(Script::EVENT) :
			{
				if (token == "EVENT")
				{
					ExtractValues(tokenized.at(a), e, S_INT);
					//Should extract priority
					if (e.ints.size() == 1)
					{
						//Store priority
						s.events.at(events).priority = e.ints.at(0);
					}
					else
					{
						//User may have forgotten about priority, assign default (1)
						s.events.at(events).priority = 1;
					}
					s.state = Script::CONDITION;
					handled = true;
				}
				break;
			}
			case(Script::CONDITION) :
			{
				if (token == "CONDITIONCOUNT")
				{
					ExtractValues(tokenized.at(a), e, S_INT);
					//Should extract count
					if (e.ints.size() == 1)
					{
						//Store count
						s.events.at(events).conditionCount = e.ints.at(0);
						conditions = 0;
						handled = true;
					}
					else
						s.state = Script::FAILED;

					if (s.events.at(events).conditionCount == 0)
						s.state = Script::ACTION;
				}
				else if (token == "CONDITION")
				{
					//SYNTAX: CONDITION KEY(IF/WHILE) OBJECT(MAIN_CAMERA, PLAYER) PROPERTY(SCALE, POSITION) =(OR !=) VALUE(INT/FLOAT/STRING)
					//TODO implement conditions
					conditions++;
				}
				break;
			}
			case(Script::ACTION) :
			{
				if (token == "ACTIONCOUNT")
				{
					ExtractValues(tokenized.at(a), e, S_INT);
					//Should extract count
					if (e.ints.size() == 1)
					{
						//Store count
						actions = 0;
						s.events.at(events).actionCount = e.ints.at(0);
						for (int b = 0; b < e.ints.at(0); b++)
							s.events.at(events).actions.push_back(Action());
						handled = true;
					}
					else
						s.state = Script::FAILED;

					if (s.events.at(events).actionCount == 0)
						s.state = Script::CHAIN;
				}
				else if (token == "ACTION")
				{
					ExtractValues(tokenized.at(a), e, S_ACTION);
					s.events.at(events).actions.at(actions).command = e.keyword.at(0);
					s.events.at(events).actions.at(actions).ints = e.ints;
					s.events.at(events).actions.at(actions).floats = e.floats;
					s.events.at(events).actions.at(actions).strings = e.strings;
					actions++;
					if (actions == s.events.at(events).actionCount)
					{
						events++;
						if (events < s.eventCount)
							s.state = Script::EVENT;
						handled = true;
					}
				}
				break;
			}
			case(Script::CHAIN) :
			{
				if (token == "CHAIN")
				{

				}
				break;
			}
			case(Script::END):
				{
				handled = true;
				break;
			}
			default:
				handled = false;
			}
			if(handled)
				tokenized.at(a).at(0).clear(); //Prevents duplicates
			if (s.state == Script::FAILED)
				return chained;
		}
	}
	//Run sort to order events by their priority
	//If priorities are the same for some events, they will execute together
	//That being said, if both events affect the same object(s), the first read event occurs first
	s.sort();

	//Quickly update/execute any events ready immediately
	s.update();

	return chained;
}
