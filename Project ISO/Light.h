#pragma once
#include <SFML/Graphics.hpp>

class Light
{
public:
	Light(bool _spot = false)
	{
		color = sf::Color::White;
		radius = 0;
		angleSpread = sf::Vector2f(0,0);
		position = sf::Vector3f(0, 0, 0);
		angle = sf::Vector2f(0,0);
		spotlight = _spot;
	}

	//Vars
	sf::Vector3f position; //Light position
	sf::Color color;
	float radius; //How far light will shine to (Spherically if not spotlight)
	sf::Vector2f angleSpread; //Spread of light (Creates arc)(xy, z)
	sf::Vector2f angle; //Where the light is pointing(xy, z)
	bool spotlight; //If the light is a point or spot
};