#pragma once
#include "TextureDictionary.h"

class GUI
{
public:
	enum ButtonStatus //Which button was clicked
	{
		Exit,
		Load,
		New,
		None,
		Save,
		Function,
		Gridlines,
		UnderGround,
		Off //If mouse is not on gui
	};
private:
	//Buttons in the interface
	class Button
	{
	public:
		sf::Vector2f size;
		sf::Vector2f position;
		char* message;
		Textures::ID id;
		ButtonStatus type;
		bool toggle; //Indicates if button is toggable
		bool state; //Holds toggle state
	};
private:
	Button *buttons;
	int buttonCount;
	int moused; //Stores id of moused over button (-1 is none)
	bool held;
	int timer; //Keeps track of how long you mouse over a button
	sf::Font tooltipFont;
	//CUSTOMIZABLE GUI CONSTS
	const int tooltipTimer = 1200; //How long before tooltip message appears
	const int fadeTimer = 500; //Milliseconds to fade in completely
	const int fadeAlpha = 220; //From 0  to 255 (color of alpha comp.)
public:
	GUI()
	{
		buttons = nullptr;
		tooltipFont.loadFromFile("C:/Windows/Fonts/Arial.ttf");
	}

	void clear()
	{
		if (buttons != nullptr)
			delete buttons;
		buttons = nullptr;
		buttonCount = 0;
		timer = 0;
	}

	//dT stands for deltaTime, time that has passed in milliseconds
	ButtonStatus checkMouse(sf::Vector2f mousePos, int dT)
	{
		bool changed = false;
		for (int i = 0; i < buttonCount; i++)
		{
			if (mousePos.x >= buttons[i].position.x &&
				mousePos.x < buttons[i].position.x + buttons[i].size.x)
			{
				if (mousePos.y >= buttons[i].position.y &&
					mousePos.y < buttons[i].position.y + buttons[i].size.y)
				{
					changed = true;

					if (moused != i)
					{
						if (timer > tooltipTimer)
							timer = tooltipTimer;
						else
							timer = 0;
					}

					moused = i;

					if (timer >= 0)
					{
						timer += dT;
						/*
						This prevents users from holding the mouse on the button for a long time and 
						overflowing the counter.
						*/
						if (timer > tooltipTimer + fadeTimer)
							timer = tooltipTimer + fadeTimer;
					}
					if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
					{
						held = true;
						return ButtonStatus::None;
					}
					else if (held)
					{
						held = false;
						timer = -1;
						return buttons[i].type;
					}

					//If no buttons pressed then we know we are on this button but not pressing anything
					return ButtonStatus::None;
				}
			}
		}

		//Also reset timer
		timer = 0;

		if (!changed)
		{
			moused = -1;
			held = false;
		}

		return ButtonStatus::Off;
	}

	void createGUI(sf::RenderTarget &window)
	{
		if (buttons != nullptr)
			clear();

		//Temp code to make buttons appear
		buttonCount = 7;
		buttons = new Button[buttonCount];
		
		for (int i = 0; i < buttonCount; i++)
		{
			buttons[i].size = sf::Vector2f(32.f, 32.f);
			buttons[i].position = sf::Vector2f(32.f * i, 0.f);
			buttons[i].toggle = false;
			buttons[4].state = true;
			buttons[6].state = false;
		}

		//Exit button
		buttons[0].id = Textures::ID::Exit;
		buttons[0].type = ButtonStatus::Exit;
		buttons[0].message = "Exit the game";
		//New button
		buttons[1].id = Textures::ID::New;
		buttons[1].type = ButtonStatus::New;
		buttons[1].message = "Create a new default map";
		//Load button
		buttons[2].id = Textures::ID::Load;
		buttons[2].type = ButtonStatus::Load;
		buttons[2].message = "Load debug.map";
		//Save button
		buttons[3].id = Textures::ID::Save;
		buttons[3].type = ButtonStatus::Save;
		buttons[3].message = "Save debug.map (Will overwrite existing one)";
		//Gridline toggle
		buttons[4].id = Textures::ID::Gridlines;
		buttons[4].type = ButtonStatus::Gridlines;
		buttons[4].toggle = true;
		buttons[4].message = "Toggles gridlines on tiles";
		//Function
		buttons[5].id = Textures::ID::Function;
		buttons[5].type = ButtonStatus::Function;
		buttons[5].message = "Function button (Dev)";
		//Underground toggle
		buttons[6].id = Textures::ID::UnderGround;
		buttons[6].type = ButtonStatus::UnderGround;
		buttons[6].toggle = true;
		buttons[6].position.x = (1024 - 32) * 1.f;
		buttons[6].message = "Toggles underground view";
	}

	void drawGUI(sf::RenderTarget &window, TextureDictionary &dict)
	{
		//In behind the gui is a slightly gray rectangle to show GUI borders
		sf::RectangleShape r;
		r.setSize(sf::Vector2f(1024.f, 32.f));
		r.setPosition(0.f, 0.f);
		r.setFillColor(sf::Color(255, 255, 255, 100));
		window.draw(r);

		r.setPosition(0.f, window.getSize().y - 32.f);
		r.setFillColor(sf::Color::Black);
		window.draw(r);

		//Each button has a black outline on its left/right sides
		r.setSize(sf::Vector2f(38.f, 32.f)); //3px larger in each direction
		r.setFillColor(sf::Color::Black);
		for (int i = 0; i < buttonCount; i++)
		{
			r.setPosition(buttons[i].position.x - 3.f, buttons[i].position.y);
			window.draw(r);
		}

		//Now render the buttons over top
		sf::Sprite b; //Button sprite
		for (int i = 0; i < buttonCount; i++)
		{
			b.setPosition(buttons[i].position);
			b.setTexture(dict.get(buttons[i].id));
			window.draw(b);
			//If the button is a toggle button and the state is off, we display it
			if (buttons[i].toggle && !buttons[i].state)
			{
				b.setTexture(dict.get(Textures::ID::Toggle));
				window.draw(b);
			}
		}
		if (moused < 0)
			return;
		b.setPosition(buttons[moused].position);
		b.setTexture(dict.get(Textures::ID::ButtonMouse));
		window.draw(b);
		if (timer > tooltipTimer)
		{
			//Draws the tooltip for the selected button
			drawTooltip(window, moused);
		}
		if (!held)
			return;
		b.setTexture(dict.get(Textures::ID::ButtonPush));
		window.draw(b);
	}

	//Draws the tooltip for the selected button
	void drawTooltip(sf::RenderTarget &window, int buttonID)
	{
		if (timer < 0)
			return; //Escape code (timer == -1)
		
		int alpha; //Used for fading the tooltip color
		if (timer - tooltipTimer < fadeTimer) //If the fade is in progress
			//We calculate the current fade state
			alpha = (int)(((timer - tooltipTimer) / (float)fadeTimer) * (float)fadeAlpha);
		else
			alpha = fadeAlpha;

		//Get the message to display on the tooltip
		sf::Text t;
		t.setString(" " + sf::String(buttons[buttonID].message) + " ");

		//If the button is a toggle, list its state
		if (buttons[buttonID].toggle)
		{
			if (buttons[buttonID].state) //On
				t.setString(t.getString() + "(state: On) ");
			else
				t.setString(t.getString() + "(state: Off) ");
		}

		//Ready the message for rendering
		t.setFont(tooltipFont);
		t.setScale(0.6f, 0.6f);
		t.setColor(sf::Color(0, 0, 0, (int)alpha));
		//Ready the back rectangle for rendering
		sf::RectangleShape r;
		r.setSize(sf::Vector2f(t.getLocalBounds().width * 0.6f, t.getLocalBounds().height));
		//If the button is on the left of the screen, tooltip displays to the right of button
		if (buttons[buttonID].position.x < window.getSize().x / 2)
		{
			t.setPosition(buttons[buttonID].position.x + 8.f, 
				buttons[buttonID].position.y + buttons[buttonID].size.y + 7.f);
		}
		else //otherwise, tooltip displays on the left
		{
			t.setPosition(buttons[buttonID].position.x + 
				buttons[buttonID].size.x - 
				r.getSize().x - 
				8.f, 
				buttons[buttonID].size.y + 7.f);
		}
		r.setPosition(t.getPosition());
		r.setFillColor(sf::Color(252, 252, 182, (int)alpha));		

		//Now we use a triangle to show which button is selected
		sf::ConvexShape triangle;
		triangle.setPointCount(3);
		//First point is bottom middle of button
		triangle.setPoint(0, sf::Vector2f(
			buttons[buttonID].position.x + (buttons[buttonID].size.x / 2),
			buttons[buttonID].position.y + 3.f*(buttons[buttonID].size.y / 4)));
		if (buttons[buttonID].position.x < window.getSize().x / 2)
		{ //Left side - to the right first
			triangle.setPoint(1, sf::Vector2f(
				buttons[buttonID].position.x + (buttons[buttonID].size.x / 2) + 8.f,
				buttons[buttonID].position.y + buttons[buttonID].size.y + 8.f));
			//Then shift over 8 pixels
			triangle.setPoint(2, sf::Vector2f(
				buttons[buttonID].position.x + (buttons[buttonID].size.x / 2),
				buttons[buttonID].position.y + buttons[buttonID].size.y + 8.f));
		}
		else
		{ //Right side - straight down first
			triangle.setPoint(1, sf::Vector2f(
				buttons[buttonID].position.x + (buttons[buttonID].size.x / 2),
				buttons[buttonID].position.y + buttons[buttonID].size.y + 8.f));
			//Then shift over 8 pixels
			triangle.setPoint(2, sf::Vector2f(
				buttons[buttonID].position.x + (buttons[buttonID].size.x / 2) - 8.f,
				buttons[buttonID].position.y + buttons[buttonID].size.y + 8.f));
		}
		triangle.setFillColor(r.getFillColor());
		triangle.setOutlineThickness(1);
		triangle.setOutlineColor(sf::Color(0, 0, 0, (int)alpha));

		//Now the rendering code
		window.draw(triangle); //The triangle
		window.draw(r); //Background rect
		window.draw(t); //Lastly, text
	}
	
	bool &getState(int buttonID)
	{
		return buttons[buttonID].state;
	}

	~GUI()
	{
		clear();
	}
};

