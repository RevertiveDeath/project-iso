#pragma once
#include "Camera.h"
#include "TextureDictionary.h"
#include "Tile.h"
#include "Object.h"

#include <vector>
#include <shlwapi.h>
#include <tchar.h>
#include <fstream>
#include <sstream>

class Map
{
public:
	CameraManager cam;
	bool underground;
	bool gridlines;
private:
	//2D array of ground tiles
	Tile** tiles;
	int width;
	int height;
	sf::Vector2i selectedTile;
	float getMin(float x);
	float getMax(float x);
	void drawTile(int x, int y, int z, int minP, sf::Vector3f &pos, sf::Sprite &s, sf::RenderTarget &window, TextureDictionary &dict);
public:
	Map();
	Map(int, int);
	void clear();
	void clearSelected();
	void createMapT(int tilewidth, int tileheight);
	void createMap(int, int);
	void draw(sf::RenderTarget &window, TextureDictionary &dict);
	void fillTile(Tile &parent, Textures::ID fillID, int &count);
	void getTile(sf::Vector2f moousePos, bool underGround);
	Tile &getTileAt(int x, int y);
	//Save and load are integers because there are multiple possible error codes
	//And I would like to display them to the user in a custom way later
	int load(std::string name);
	int save(std::string name);
	Object *wall;
	std::string getInfo();
	sf::Vector2i getSelected() { return selectedTile; }
	sf::Vector2f getSize() { return sf::Vector2f(width * 32.f, height * 32.f); }
	Tile &getSelectedTile() { return tiles[selectedTile.x][selectedTile.y]; }
	~Map();



	//Output
	std::ostringstream stream;
};

