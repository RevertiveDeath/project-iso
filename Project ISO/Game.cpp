#include "Game.h"

Game::Game()
{
	reloadTextures();
	heldPos = sf::Vector2f(-1.f, -1.f);
	held = false;
	heldTile = nullptr;
}

Game::Game(float mapWidth, float mapHeight, sf::RenderWindow &window)
{
	reloadTextures();
	heldTile = nullptr;
	gui.createGUI(window);
	heldPos = sf::Vector2f(-1.f, -1.f);
	held = false;
	map.cam.resetMain(); //Reset main map camera
}

void Game::update(sf::RenderWindow &window, int dT)
{
	//HANDLE INPUT

	//Check that the mouse is in the window
	sf::Vector2f mousePos((float)(sf::Mouse::getPosition().x - window.getPosition().x),
		(float)(sf::Mouse::getPosition().y - window.getPosition().y - 32));

	//CAMERA MOVEMENT 
	//If mouse is at the edge of the screen (and not in the gui), scroll the map
	if (!sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) //If shift is not held
	{
		if (mousePos.x < 64.f && mousePos.y > 32.f || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			//Left edge
			if (map.cam.getPosition().x >= -80.f)
				map.cam.move(-100.f*(dT / 1000.f), 0.f);
		}
		else if (mousePos.x > window.getSize().x - 64.f && mousePos.y > 32.f || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			//Right edge
			if (map.cam.getPosition().x + map.cam.getSize().x < map.getSize().x + 120.f)
				map.cam.move(100.f*(dT / 1000.f), 0.f);
		}
		if (mousePos.y < 96.f && mousePos.y > 32.f || sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			//Top edge
			if (map.cam.getPosition().y >= -300.f)
				map.cam.move(0.f, -100.f*(dT / 1000.f));
		}
		else if (mousePos.y > window.getSize().y - 64.f || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			//Bottom edge
			if (map.cam.getPosition().y + map.cam.getSize().y < map.getSize().y + 80.f)
				map.cam.move(0.f, 100.f*(dT / 1000.f));
		}
	}

	//MOUSE CLICKS
	GUI::ButtonStatus status = gui.checkMouse(mousePos, dT);
	switch (status)
	{
	case(GUI::ButtonStatus::Exit):
	{
		//No matter the result we still exit
		window.close();
		break;
	}
	case(GUI::ButtonStatus::New):
	{
		//TODO ask user for map size
		map.createMap(1024, 768);
		gui.getState(4) = map.gridlines;
		gui.getState(6) = map.underground;
		break;
	}
	case(GUI::ButtonStatus::Load):
	{
		map.load("debug");
		gui.getState(4) = map.gridlines;
		gui.getState(6) = map.underground;
		//Info feed display result of load
		break;
	}
	case(GUI::ButtonStatus::Save):
	{
		map.save("debug");
		//Info feed display result of save
		break;
	}
	case(GUI::ButtonStatus::Gridlines):
	{
		gui.getState(4) = !gui.getState(4);
		map.gridlines = gui.getState(4);
		break;
	}
	case(GUI::ButtonStatus::Function):
	{
		//DEBUG BUTTON
		scripter.load("../Scripts/cam.isf");
		if (map.wall == nullptr)
		{
			//map.wall = new Object(map.getTileAt(2,2), Textures::ID::WallSouth);
		}
		break;
	}
	case(GUI::ButtonStatus::UnderGround):
	{
		gui.getState(6) = !gui.getState(6);
		map.underground = gui.getState(6);
		break;
	}
	case(GUI::ButtonStatus::None):
		break;
	case(GUI::ButtonStatus::Off) :
		break;
	default: //Should never be reached
		throw std::runtime_error("GUI could not handle the mouse input");
	}

	//Now map
 	if (status == GUI::ButtonStatus::Off)
		map.getTile(mousePos, gui.getState(6));
	else
		map.clearSelected();


	if(held)
		map.clearSelected();

	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
	{
		if (!held)
		{
			if (map.getSelected() != sf::Vector2i(-1, -1))
			{
				held = true;
				heldPos = sf::Vector2f((float)sf::Mouse::getPosition().x, (float)sf::Mouse::getPosition().y);
				heldTile = &map.getSelectedTile();
			}
		}
		else
		{
			if (sf::Mouse::getPosition().y < heldPos.y - 8.f)
			{ 
				while (sf::Mouse::getPosition().y < heldPos.y - 8.f && heldTile->getPosition().z < 32.f)
				{
					heldPos.y -= 8.f;
					heldTile->setZ(heldTile->getPosition().z + 1.f);
				}
			}
			else if (sf::Mouse::getPosition().y > heldPos.y + 8.f)
			{
				while (sf::Mouse::getPosition().y > heldPos.y + 8.f && heldTile->getPosition().z > 0.f)
				{
					heldPos.y += 8.f;
					heldTile->setZ(heldTile->getPosition().z - 1.f);
				}
			}
		}
	}
	else
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Middle))
		{
			if (!changeHeld)
			{
				if (map.getSelected() != sf::Vector2i(-1, -1))
				{
					if (map.getSelectedTile().getImageID() == Textures::ID::Grass)
						map.getSelectedTile().setImageID(Textures::ID::Wood);
					else if (map.getSelectedTile().getImageID() == Textures::ID::Wood)
						map.getSelectedTile().setImageID(Textures::ID::Cobble);
					else
						map.getSelectedTile().setImageID(Textures::ID::Grass);
				}
				changeHeld = true;
			}
		}
		else
			changeHeld = false;

		held = false;
		heldPos = sf::Vector2f(-1.f, -1.f);
	}

	//UPDATE GAME LOGIC
	scripter.update();

	for (int i = 0; i < scripter.active.events.size(); i++)
		if (scripter.active.events.at(i).execute)
			execute(scripter.active.events.at(i));

	for (int i = scripter.active.events.size() - 1; i >= 0; i--)
		if (scripter.active.events.at(i).execute)
			scripter.active.events.erase(scripter.active.events.begin() + i);
}

//Executes scripted event
void Game::execute(Event e)
{
	for (int i = 0; i <  e.actions.size(); i++)
	{
		if (e.actions.size() == 0)
			return;
		Action a = e.actions.at(i);
		if (a.command == "LOAD_MAP")
		{
			map.load(a.strings[0]);
		}
		else if (a.command == "SAVE_MAP")
		{
			map.save(a.strings[0]);
		}
		else if (a.command == "CREATE_MAP")
		{
			map.createMap(a.ints[0], a.ints[1]);
			map.save(a.strings[0]);
		}
		else if (a.command == "MOVE_CAMERA")
		{
			map.cam.setPosition(sf::Vector2f(float(a.ints[0]), float(a.ints[1])));
		}
		else if (a.command == "MOVE_PLAYER")
		{
			//Move player
		}
		else if (a.command == "QUIT" || a.command == "EXIT")
		{
			//Exit app
		}
		else
		{
			//Action invalid (not executed but still removed)
			//To prevent hangtime we remove an invalid command and put a message out explaining that the command was not recognized

			//TODO send message to screen console
		}
	}

	//Action executed, erase
	for (int i = e.actions.size() - 1; i >= 0; i--)
		e.actions.erase(e.actions.begin() + i);
}

void Game::reloadTextures()
{
	//clears old textures
	textures.clear();
	//loads new ones

	//GUI
	textures.load(Textures::ID::ButtonPush, "GUI/buttonpush.png");
	textures.load(Textures::ID::ButtonMouse, "GUI/buttonmouse.png");
	textures.load(Textures::ID::Compass, "GUI/compass.png");
	textures.load(Textures::ID::Exit, "GUI/exit.bmp");
	textures.load(Textures::ID::New, "GUI/new.bmp");
	textures.load(Textures::ID::Load, "GUI/load.bmp");
	textures.load(Textures::ID::Save, "GUI/save.bmp");
	textures.load(Textures::ID::Gridlines, "GUI/gridlines.bmp");
	textures.load(Textures::ID::Function, "GUI/function.bmp");
	textures.load(Textures::ID::UnderGround, "GUI/underground.bmp");
	textures.load(Textures::ID::Mouse, "GUI/mouse.png");
	textures.load(Textures::ID::Move, "GUI/mouse_move.png");
	textures.load(Textures::ID::Toggle, "GUI/toggle.png");
	//Tile
	textures.load(Textures::ID::Blank, "Tile/blank.png");
	textures.load(Textures::ID::Cobble, "Tile/cobble.png");
	textures.load(Textures::ID::Ground, "Tile/groundwall_lines.png");
	textures.load(Textures::ID::Grass, "Tile/grass.png");
	textures.load(Textures::ID::GridBlack, "Tile/grid.png");
	textures.load(Textures::ID::GridN, "Tile/GridN.png");
	textures.load(Textures::ID::GridE, "Tile/GridE.png");
	textures.load(Textures::ID::GridS, "Tile/GridS.png");
	textures.load(Textures::ID::GridW, "Tile/GridW.png");
	textures.load(Textures::ID::GridWhite, "Tile/white.png"); 
	textures.load(Textures::ID::Shadow, "Tile/shadow.png");
	textures.load(Textures::ID::Transparent, "Tile/transparentwall.png");
	textures.load(Textures::ID::Wood, "Tile/wood.png");
	textures.load(Textures::ID::Strut, "Tile/wood_supp.png");
	//Objects
	textures.load(Textures::ID::WallSouth, "Objects/Structural/wall_south.png");
}

void Game::render(sf::RenderTarget &window, sf::Vector2f mouse)
{
	map.draw(window, textures);
	gui.drawGUI(window, textures);
	
	//Now we draw the mouse/tooltip
	sf::Sprite s, t;

	if (held && heldTile != nullptr)
	{
		//Show manipulated tile
		t.setPosition(heldTile->getSpritePosition() - map.cam.getPosition());
		t.setTexture(textures.get(Textures::ID::GridWhite));
		window.draw(t);
		s.setTexture(textures.get(Textures::ID::Move));
	}
	else
		s.setTexture(textures.get(Textures::ID::Mouse));
	s.setPosition(mouse);
	window.draw(s);
}
