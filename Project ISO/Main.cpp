#ifdef SFML_STATIC
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "freetype.lib")
#pragma comment(lib, "jpeg.lib")
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "gdi32.lib")  
#endif // SFML_STATIC

#include "Game.h"

int main()
{
	//INITIALIZE GAME AND RENDER WINDOW
	float width = 1024;
	float height = 768;
	bool focus = true;
	sf::RenderWindow window(sf::VideoMode((int)width, (int)height), "Project ISO", sf::Style::Resize); // , sf::Style::Fullscreen/Resize;
	window.setVerticalSyncEnabled(true);
	window.setMouseCursorVisible(false);

	//Manages frames and updates
	sf::Clock clock;
	sf::Time deltaTime;

	//Displays debug information
	sf::Font font;
	font.loadFromFile("C:/Windows/Fonts/Arial.ttf");
	sf::Text text("0", font);
	text.setScale(sf::Vector2f(0.8f, 0.8f));
	text.setPosition(4.f, window.getSize().y - text.getLocalBounds().height - 8.f);

	//Create the game management object
	Game game(width, height, window);

	//Game loop (main)
	while (window.isOpen())
	{
		//Save the current delta time and restart counter
		deltaTime = clock.restart();

		//Handle events
		sf::Event event;
		while (window.pollEvent(event))
		{
			if(event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::GainedFocus)
				focus = true;
			if (event.type == sf::Event::LostFocus)
				focus = false;
			if (event.type == sf::Event::Resized)
				window.setSize(sf::Vector2u((int)width, (int)height));
		}

		//Grab the current mouse position
		sf::Vector2f mouse((float)(sf::Mouse::getPosition().x - window.getPosition().x),
			(float)(sf::Mouse::getPosition().y - window.getPosition().y - 32));

		//If the window is focused, we handle user input and pass it our deltaTime
		if(focus)
			game.update(window, deltaTime.asMilliseconds());

		//TEMP shows selected tile (3d coords)
		std::stringstream ss;
		ss << game.getInfo();
		text.setString(ss.str());

		//Display code
		window.clear(); //Clear last frame
		game.render(window, mouse); //Render the game state
		window.draw(text); //TEMP draw debug info
		//Border the screen
		sf::RectangleShape r;
		r.setFillColor(sf::Color::Black);
		r.setSize(sf::Vector2f((float)window.getSize().x, 1.f));
		r.setPosition(0.f, 0.f);
		window.draw(r);
		r.setSize(sf::Vector2f(1.f, (float)window.getSize().y));
		window.draw(r);
		r.setPosition(window.getSize().x - 1.f, 0.f);
		window.draw(r);
		r.setPosition(0.f, (float)window.getSize().y - 1.f);
		r.setSize(sf::Vector2f((float)window.getSize().x, 1.f));
		window.draw(r);
		window.display(); //Display the window
	}

	//End of main
	return 0;
}