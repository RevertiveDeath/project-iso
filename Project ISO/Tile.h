#pragma once

class Tile
{
public:
	//Keeps track of tile types/states
	enum State
	{
		Overgrown, //Overgrown grass tile
		Weeds, //Larger sprouts
		Sprouts, //Tufts of weeds sprouting
		Normal, //Well trimmed grass with no mowing marks
		Mowed, //Freshly cut grass with mowing marks
	};
private:
	const static int maxGrowthTime = 5000; //5 seconds per growth percent
	Textures::ID image;
	Tile::State state;
	sf::Vector3f position;
	sf::Vector2i coords;
	//Tracks weed growth of current tile
	//(For desert tiles, this does not affect anything)
	int growth;
	int growthTime;
	/*
	GROWTH INDEX:
	After mowing, set to 0
	0->29 is mowed
	30->59 is normal
	60->79 is sprouts
	80->99 is weeds
	100 is overgrown

	*/
public:
	Tile(float newX, float newY, float newZ, Textures::ID newID)
	{
		growthTime = 0;
		growth = 30;
		state = State::Normal;
		image = newID;
		position = sf::Vector3f(newX, newY, newZ);
	}
	Tile(float newX, float newY, float newZ)
	{
		growthTime = 0;
		growth = 30;
		state = State::Normal;
		image = Textures::ID::Blank;
		position = sf::Vector3f(newX, newY, newZ);
	}
	Tile()
	{
		growthTime = 0;
		growth = 30;
		state = State::Normal;
		image = Textures::ID::Blank;
		position = sf::Vector3f(0.f, 0.f, 0.f);
	}

	void setImageID(Textures::ID newImageID)
	{
		image = newImageID;
	}
	void setCoords(int newX, int newY)
	{
		coords = sf::Vector2i(newX, newY);
	}
	void setPosition(float newX, float newY, float newZ)
	{
		setPosition(sf::Vector3f(newX, newY, newZ));
	}
	void setPosition(sf::Vector3f newPosition)
	{
		position = newPosition;
	}
	void setState(Tile::State newState)
	{
		state = newState;
	}
	void setZ(float newZ)
	{
		//Set a z-coordinate only
		position.z = newZ;
	}

	Textures::ID getImageID()
	{
		return image;
	}
	sf::Vector2i getCoords()
	{
		return coords;
	}
	sf::Vector3f getPosition()
	{
		return position;
	}
	sf::Vector2f getSpritePosition()
	{
		return sf::Vector2f(position.x, position.y - (8.f * position.z));
	}
	Tile::State getState()
	{
		return state;
	}

	void save(std::ofstream &out)
	{
		out << TextureDictionary::getInt(image) << std::endl
			<< position.x << std::endl
			<< position.y << std::endl
			<< position.z << std::endl;
	}

	void update()
	{
		growthTime++;
		if (growthTime >= maxGrowthTime)
		{
			growthTime = 0;
			growth++;
			if (growth > 29)
				image = Textures::ID::Grass;
			else if (growth > 59)
				image = Textures::ID::Sprouts;
			else if (growth > 79)
				image = Textures::ID::Weeds;
			else
			{
				growth = 100;
				image = Textures::ID::Overgrown;
			}
		}
	}
};
