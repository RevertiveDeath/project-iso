#pragma once
#include "Light.h"
#include "Map.h"
#include <vector>

class LightEngine
{
public:
	LightEngine(Map &_map) : map(_map){}
	//Step calls ShineLight for each light 
	void Step(sf::RenderWindow &window);
	std::vector <Light> Lights;
	Map &map;
private: 
	void ShineLight(Light &light, sf::RenderWindow &window);
	static const float Distance(const sf::Vector3f &p1, const sf::Vector3f &p2);
	static const sf::Vector2f GetCenter(const sf::FloatRect &fr); //Get center of a rectangle

	//If a light's radius manages to intersect multiple tiles, we need to find the shortest distance to shorten the light
	struct FindDistance 
	{
		FindDistance();
		float shortest;
		bool LightHitsTile(Light &l, Tile &tile, const sf::Vector2f cur_ang, float &refLength);
		bool start; //To get forst distance to refer off of
	};

	FindDistance findDis;
};