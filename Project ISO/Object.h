#pragma once
#include "TextureDictionary.h"
#include "Tile.h"

class Object
{
private:
	//Object requires parent tile for storing its location on the map
	Tile &parent;
	Textures::ID texture;
public:
	//Initialize parent tile in constructor
	Object(Tile &_parent) : parent(_parent)
	{
		
	}
	Object(Tile &_parent, Textures::ID _texture) : parent(_parent)
	{
		load(_texture);
	}
	//Sets the object texture
	void load(Textures::ID _texture)
	{
		texture = _texture;
	}
	Textures::ID Texture()
	{
		return texture;
	}
	Tile &Parent()
	{
		return parent;
	}
	void load(std::ifstream stream)
	{

	}
	void save(std::ofstream stream)
	{

	}
};