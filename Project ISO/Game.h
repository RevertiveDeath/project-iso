#pragma once
#include "GUI.h"
#include "Map.h"
#include "Parser.h"

class Game
{
private:
	GUI gui;
	Map map;
	TextureDictionary textures;
	sf::Vector2f heldPos;
	Tile *heldTile;
	bool held, changeHeld;
	Parser scripter;
public:
	Game();
	Game(float mapWidth, float mapHeight, sf::RenderWindow &window);

	void execute(Event e);
	void update(sf::RenderWindow &window, int dT);
	void reloadTextures();
	void render(sf::RenderTarget &window, sf::Vector2f mouse);

	//TEMP
	std::string getInfo() 
	{
		std::ostringstream stream;
		stream << map.getInfo();
		return stream.str();
	}
};
