#pragma once
//SFML lib
#include <SFML\Graphics.hpp>
//C++ libs3
#include <assert.h>
#include <map>
#include <memory>
#include <istream>
#include <fstream>

namespace Textures
{
	enum class ID
	{
		Blank,
		ButtonMouse,
		ButtonPush,
		Cobble,
		Compass,
		Exit,
		Function,
		Grass,
		GridBlack,
		Gridlines,
		GridWhite,
		GridN,
		GridE,
		GridS,
		GridW,
		Ground,
		Load,
		Mouse,
		Move,
		New,
		Overgrown,
		Save,
		Shadow,
		Sprouts,
		Strut,
		Toggle,
		Transparent,
		UnderGround,
		Weeds,
		Wood,
		//Objects
		WallSouth
	};
}

class TextureDictionary
{
private:
	std::map < Textures::ID,
		std::unique_ptr < sf::Texture >> textureMap;
	
public:
	void load(Textures::ID id, const std::string& filename);
	void clear();

	sf::Texture& get(Textures::ID id);
	const sf::Texture& get(Textures::ID id) const;

	static Textures::ID getID(int i);
	static int getInt(Textures::ID id);
};
