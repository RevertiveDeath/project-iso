#pragma once
#include "Map.h"

enum Angle //Cardinal directions
{
	North, //Facing top right
	South, //Facing bottom left
	East, //Facing bottom right
	West, //Facing top left
};

//Class for buildings, stalls, entrances/exits, etc.
class Building
{
private:
	Tile *baseTile;
	float height; //Height above base tile (1.0 unit / 8px)
	Angle faceAngle; //Direction building is facing
public:
	Building()
	{
		baseTile = nullptr;
		height = 0.f;
		faceAngle = Angle::North;
	}
	Building(Tile *newBase, float newHeight, Angle newAngle)
	{
		baseTile = newBase;
		height = newHeight;
		faceAngle = newAngle;
	}
	~Building()
	{
		//Sets baseTile pointer to null (we don't want to delete the base)
		baseTile = nullptr;
	}

	void setBaseTile(Tile *newBase)
	{
		baseTile = newBase;
	}
	Tile *getBaseTile()
	{
		return baseTile;
	}

	void setAngle(Angle newAngle)
	{
		faceAngle = newAngle;
	}
	Angle getAngle()
	{
		return faceAngle;
	}

	void setHeight(float newHeight)
	{
		height = newHeight;
	}
	float getHeight()
	{
		return height;
	}
};

