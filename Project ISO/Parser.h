//This file parses a script file
#include "Script.h"
#include "Token.h"
#include <fstream>


class Parser
{
public:
	Script active;

	Parser()
	{

	}
	Parser(std::string &scriptname)
	{
		load(scriptname);
	}
	//Loads script from file and parses it to the script object
	void load(std::string fileName)
	{
		std::cout << "Loading " + fileName << std::endl;
		std::ifstream file(fileName.c_str());
		if (file.is_open() && file.good())
		{
			std::vector<std::string> input;
			std::string temp;
			while (getline(file, temp))
			{
				input.push_back(temp);
			}
			file.close();

			//TODO allow chaining of scripts
			std::vector<std::string> chained = Tokenize(active, input);		
			return;
		}
		active.state = Script::FAILED;
	}

	//Updates active script(s)
	void update()
	{
		active.update();
	}
};