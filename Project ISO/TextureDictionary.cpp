#include "TextureDictionary.h"

void TextureDictionary::load(Textures::ID id, const std::string& filename)
{
	//Path is relative to executable location
	//Therefore we must deposit the final exe in a bin/build folder
	//Adjacent to the images folder
	std::string imagePath = "../Images/";
	std::unique_ptr<sf::Texture> texture(new sf::Texture());
	if (!texture->loadFromFile(imagePath + filename))
		throw std::runtime_error(
			"Texture loading failed for file: " + filename);

	auto inserted = textureMap.insert(std::make_pair(id, std::move(texture)));
	//If the specified ID is already in use, inserted's second value will be true
	//This is a logical error:
	assert(inserted.second);
}

void TextureDictionary::clear()
{
	textureMap.clear();
}

sf::Texture& TextureDictionary::get(Textures::ID id)
{
	auto found = textureMap.find(id);

	//If not found, this assertion will result in a logical error
	assert(found != textureMap.end());

	//Since found will return the map's pair
	//We return the second item in the pair (An sf::Texture ref)
	return *found->second;
}

const sf::Texture& TextureDictionary::get(Textures::ID id) const
{
	auto found = textureMap.find(id);
	//Since found will return the map's pair
	//We return the second item in the pair (An sf::Texture ref)
	return *found->second;
}

Textures::ID TextureDictionary::getID(int i)
{
	switch (i)
	{
	case(0) :
		return Textures::ID::Grass;
	case(1) :
		return Textures::ID::Wood;
	case(2) :
		return Textures::ID::Cobble;
	default:
		return Textures::ID::Blank;
	}
}

int TextureDictionary::getInt(Textures::ID id)
{
	if (id == Textures::ID::Grass)
		return 0;
	else if (id == Textures::ID::Wood)
		return 1;
	else if (id == Textures::ID::Cobble)
		return 2;
	else
		return -1;
}