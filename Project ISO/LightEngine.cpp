#include "LightEngine.h"

LightEngine::FindDistance::FindDistance()
{
	start = false;
	shortest = 0;
}

const sf::Vector2f LightEngine::GetCenter(const sf::FloatRect &fr)
{
	return sf::Vector2f(fr.left + (fr.width / 2), fr.top + (fr.height / 2));
}

const float LightEngine::Distance(const sf::Vector3f &p1, const sf::Vector3f &p2)
{
	float x = p2.x - p1.x;
	float y = p2.y - p1.y;
	float z = p2.z - p1.z;

	return sqrt((x * x) + (y * y) + (z * z));
}

void LightEngine::Step(sf::RenderWindow &window)
{
	for (unsigned i = 0; i < Lights.size(); i++)
	{
		ShineLight(Lights[i], window); //Shine all lights
	}
}

void LightEngine::ShineLight(Light &light, sf::RenderWindow &window)
{
	sf::Vector2f current_angle = light.angle - (light.angleSpread / 2.f);

	//Dynamic length of light, changable in Light hits Tile
	float dyn_len = light.radius;
	float addto = 1.f / light.radius;
}