#include "Map.h"

Map::Map()
{
	//Arbitrary default map size
	tiles = nullptr;
	wall = nullptr;
	selectedTile = sf::Vector2i(-1, -1); //None selected
}

//Width and height are number of pixels in map
Map::Map(int pixelwidth, int pixelheight)
{
	tiles = nullptr;
	wall = nullptr;
	selectedTile = sf::Vector2i(-1, -1); //None selected
	cam.resetMain();
	cam.setSize(sf::Vector2f(1024, 768));
	createMap(pixelwidth, pixelheight);
}

Tile &Map::getTileAt(int x, int y)
{
	return tiles[x][y];
}

void Map::clear()
{
	if (tiles != nullptr)
	{
		for (int i = 0; i < width; i++)
		{
			if (tiles[i] != nullptr)
				delete tiles[i];
			tiles[i] = nullptr;
		}
		delete tiles;
		tiles = nullptr;
	}
	width = 0;
	height = 0;
}

void Map::clearSelected()
{
	selectedTile = sf::Vector2i(-1, -1); //None selected
}

void Map::createMapT(int tilewidth, int tileheight)
{
	createMap(tilewidth * 32, tileheight * 32);
}

void Map::createMap(int pixelwidth, int pixelheight)
{
	clear();
	gridlines = false;
	underground = false;
	//We need to start with height
	//Formula for number of vertical tiles in array is:(modHeight / 32)*2 - 1
	int modWidth = pixelwidth;
	while (modWidth % 64 != 0)
		modWidth--;
	if (modWidth == 0)
		modWidth = 640;
	int modHeight = pixelheight;
	while (modHeight % 32 != 0)
		modHeight--;
	if (modHeight == 0)
		modHeight = 480;
	
	width = (modWidth / 64) + ((modWidth / 64) - 1);
	height = (modHeight / 32);

	//Now create the array
	tiles = new Tile*[width];
	//And initialize each tile
	for (int i = 0; i < width; i++)
		tiles[i] = new Tile[height];

	//Now we set defaults vals for each tile (and positions)
	//Outer rows are forced to be Invalid
	cam.setSize(sf::Vector2f(1024.f,768.f));
	cam.setPosition(sf::Vector2f(0.f, 0.f));
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			//The +256 is to center the map widthwise, the 192 is heightwise, the 64 is because of the 8.f z-depth
			tiles[x][y].setPosition((float)(32 * x), (float)(32 * (y - 1) + 16 * (x % 2)), 8.f);
			tiles[x][y].setImageID(Textures::ID::Grass);	
			tiles[x][y].setCoords(x, y);
			if (y == 0 || y == height - 1 || x <= 1|| x >= width - 2)
				tiles[x][y].setImageID(Textures::ID::Wood);
		}		
	}
}

void Map::draw(sf::RenderTarget &window, TextureDictionary &dict)
{
	//Drawing sprite
	sf::Sprite s;

	//Values used to determine loop bounds based on main camera
	int y1 = 0;
	int y2 = height;
	int x1 = 0;
	int x2 = width;

	if (width > 0 && height > 0)
	{
		//Start loc
		while (tiles[x1][0].getSpritePosition().x < cam.getPosition().x)
		{
			x1++;
			if (x1 > width)
			{
				x1 = 0;
				break;
			}
		}
		while (tiles[0][y1].getSpritePosition().y < cam.getPosition().y)
		{
			y1++;
			if (y1 > height)
			{
				y1 = 0;
				break;
			}
		}
		//End locaction
		//For x direction we add an extra 2 to have a bit of pre-rendered tile
		x2 = x1 + 2 + cam.getTileSize().x;
		//In the y direction we have 5 extra tiles for tiles at maximum height cap
		y2 = y1 + 5 + cam.getTileSize().y;

		//Shift over now
		x1-=2;
		y1-=3;

		//Now we verify that the camera doesn't exceed the map, if it does, we force center the camera
		if (x1 < 0)
			x1 = 0;
		if (y1 < 0)
			y1 = 0;

		if (x2 > width)
			x2 = width;
		if (y2 > height)
			y2 = height;
	}

	if (x1 % 2 != 0)
		x1--; //Have to start on even tile space

	for (int y = y1; y < y2; y++)
	{
		//The below x tile code is split into two loops because
		//Odd x tiles (x = 1->width) are in front of even ones
		for (int startX = x1; startX <= x1+1; startX++)
		{
			for (int x = startX; x < x2; x += 2)
			{
				int z = 0;
				int minY = -1;
				if (!underground)
				{
					//Tile render optimization code
					z = 0;
					minY = (int)tiles[x][y].getSpritePosition().y - 8; //Minimum y pixel value to be rendered for tiles[x][y]
					int left, center, right, max;
					if (x > 0 && x < (width - 1) && y < (height - 1))
					{
						if (x % 2 == 0) //Even
						{
							for (int tY = y; tY < height; tY++)
							{
								if (tY == y) //First pass
								{
									//Check (x-1,tY) and (x+1,tY)
									if (tiles[x - 1][tY].getSpritePosition().y > tiles[x + 1][tY].getSpritePosition().y)
										minY = (int)tiles[x - 1][tY].getSpritePosition().y;
									else
										minY = (int)tiles[x + 1][tY].getSpritePosition().y;
								}
								else
								{
									//Check (x-1,tY) (x,tY) and (x+1,tY)
									left = (int)tiles[x - 1][tY].getSpritePosition().y;
									center = (int)tiles[x][tY].getSpritePosition().y;
									right = (int)tiles[x + 1][tY].getSpritePosition().y;
									max = right;
									if (left > right)
										max = left;
									if (max > center)
										max = center;
									//Max is maximum pixel now
									if (max < minY)
										minY = max;
								}
							}

						}
						else //Odd
						{
							for (int tY = (y + 1); tY < height; tY++)
							{
								left = (int)tiles[x - 1][tY].getSpritePosition().y;
								center = (int)tiles[x][tY].getSpritePosition().y;
								right = (int)tiles[x + 1][tY].getSpritePosition().y;
								max = right;
								if (left > right)
									max = left;
								if (max > center)
									max = center;
								//Max is maximum pixel now
								if (max < minY)
									minY = max;
							}
						}
					}

					//Checks if tile need not be rendered
					if (minY < (int)tiles[x][y].getSpritePosition().y - 8)
						z = -1;
					if (minY == (int)tiles[x][y].getSpritePosition().y - 8)
						minY = -1;
					else
						minY += 8;
				}

				drawTile(x, y, z, minY, tiles[x][y].getPosition(), s, window, dict);
				if (x == 2 && y == 2 && wall != nullptr)
				{
					sf::Sprite s;
					s.setPosition(wall->Parent().getSpritePosition() - cam.getPosition());
					s.setTexture(dict.get(wall->Texture()));
					window.draw(s);
				}
			}
		}
	}

	//Display main camera location at bottom
	stream = std::ostringstream();
	stream << "Camera Location: " << cam.getPosition().x << ", " << cam.getPosition().y;
}

std::string Map::getInfo()
{
	return stream.str();
}

void Map::drawTile(int x, int y, int z, int minP, sf::Vector3f &pos, sf::Sprite &s, sf::RenderTarget &window, TextureDictionary &dict)
{
	//z = -1 is escape code
	if (z < 0)
		return;
	//Position contains x, y and z components for vertical translation
	//Elevated tiles are rendered with the ground views as well
	sf::Color old = s.getColor();
	sf::Color transparent = sf::Color(255, 255, 255, 200);
	if (pos.z > 0)
	{ 
		if (underground)
			s.setColor(transparent);

		s.setTexture(dict.get(Textures::ID::Ground));
	
		for (int zt = z; zt < (int)pos.z; zt++)
		{
			//when minP == -1, we draw all ground for all tiles (underground view)
			//This if statement ensures that we only render visible ground segments
			if (pos.y - zt*8.f < minP || minP == -1)
			{
				s.setPosition(sf::Vector2f(pos.x - cam.getPosition().x, pos.y - cam.getPosition().y - zt*8.f));
				window.draw(s);
			}
		}
	}

	s.setPosition(tiles[x][y].getSpritePosition() - cam.getPosition());
	s.setTexture(dict.get(tiles[x][y].getImageID()));
	window.draw(s);

	if (gridlines)
	{
		s.setColor(old);
		s.setTexture(dict.get(Textures::ID::GridBlack));
		window.draw(s);
	}
	else
	{
		if (x % 2 != 0)
		{
			if (tiles[x - 1][y].getPosition().z != pos.z)
			{
				s.setTexture(dict.get(Textures::ID::GridW));
				window.draw(s);
			}		
			if (tiles[x + 1][y].getPosition().z != pos.z)
			{
				s.setTexture(dict.get(Textures::ID::GridN));
				window.draw(s);
			}
			if (y < height - 1)
			{
				if (tiles[x + 1][y + 1].getPosition().z != pos.z)
				{
					s.setTexture(dict.get(Textures::ID::GridE));
					window.draw(s);
				}
				if (tiles[x - 1][y + 1].getPosition().z != pos.z)
				{
					s.setTexture(dict.get(Textures::ID::GridS));
					window.draw(s);
				}
			}
			else
			{
				s.setTexture(dict.get(Textures::ID::GridE));
				window.draw(s);
				s.setTexture(dict.get(Textures::ID::GridS));
				window.draw(s);
			}
		}
		else
		{
			if (y > 0)
			{
				if (x > 0)
				{
					if (tiles[x - 1][y - 1].getPosition().z != pos.z)
					{
						s.setTexture(dict.get(Textures::ID::GridW));
						window.draw(s);
					}
					if (tiles[x - 1][y].getPosition().z != pos.z)
					{
						s.setTexture(dict.get(Textures::ID::GridS));
						window.draw(s);
					}
				}
				else
				{
					s.setTexture(dict.get(Textures::ID::GridW));
					window.draw(s);
					s.setTexture(dict.get(Textures::ID::GridS));
					window.draw(s);
				}
				if (x < width - 1)
				{
					if (tiles[x + 1][y - 1].getPosition().z != pos.z)
					{
						s.setTexture(dict.get(Textures::ID::GridN));
						window.draw(s);
					}
					if (tiles[x + 1][y].getPosition().z != pos.z)
					{
						s.setTexture(dict.get(Textures::ID::GridE));
						window.draw(s);
					}
				}
				else
				{
					s.setTexture(dict.get(Textures::ID::GridN));
					window.draw(s);
					s.setTexture(dict.get(Textures::ID::GridE));
					window.draw(s);
				}
			}
			else
			{
				s.setTexture(dict.get(Textures::ID::GridW));
				window.draw(s);
				s.setTexture(dict.get(Textures::ID::GridN));
				window.draw(s);
			}
		}
	}

	//Display the moused over tile
	if (sf::Vector2i(x, y) == selectedTile)
	{
		s.setTexture(dict.get(Textures::ID::Shadow));
		window.draw(s);
	}
}

void Map::fillTile(Tile &parent, Textures::ID fillID, int &count)
{
	return; //Disabled
}

void Map::getTile(sf::Vector2f mousePos, bool underGround)
{
	mousePos += cam.getPosition();
	//Account for tiles being in front of other tiles by disallowing selecting back ones unless underground view is enabled
	float tX;
	float tY;

	//Values used to determine loop bounds based on main camera
	int y1 = 0;
	int y2 = height;
	int x1 = 0;
	int x2 = width;

	if (width > 0 && height > 0)
	{
		//Start loc
		while (tiles[x1][0].getSpritePosition().x < cam.getPosition().x)
		{
			x1++;
			if (x1 > width)
			{
				x1 = 0;
				break;
			}
		}
		while (tiles[0][y1].getSpritePosition().y < cam.getPosition().y)
		{
			y1++;
			if (y1 > height)
			{
				y1 = 0;
				break;
			}
		}
		//End locaction
		//For x direction we add an extra 2 to have a bit of pre-rendered tile
		x2 = x1 + 2 + cam.getTileSize().x;
		//In the y direction we have 5 extra tiles for tiles at maximum height cap
		y2 = y1 + 5 + cam.getTileSize().y;

		//Shift over now
		x1 -= 2;
		y1 -= 3;

		//Now we verify that the camera doesn't exceed the map, if it does, we force center the camera
		if (x1 < 0)
			x1 = 0;
		if (y1 < 0)
			y1 = 0;

		if (x2 > width)
			x2 = width;
		if (y2 > height)
			y2 = height;
	}

	if (x1 % 2 != 0)
		x1--;

	selectedTile = sf::Vector2i(-1, -1);
	bool valid;
	//We will go to 4 tiles past start x
	for (int y = y2; y >= y1; y--)
	{
		for (int startX = x1 + 1; startX >= x1; startX--)
		{
			for (int x = startX; x < x2; x += 2)
			{
				tX = tiles[x][y].getPosition().x;
				tY = tiles[x][y].getSpritePosition().y + 32.f;
				if (mousePos.x >= tX && mousePos.x < tX + 64.f)
				{
					if (mousePos.y >= tY && mousePos.y < tY + 32.f)
					{
						if (getMin(mousePos.x - tX) <= mousePos.y - tY &&
							mousePos.y - tY <= getMax(mousePos.x - tX))
						{
							valid = true;
							//We know that we are in tile (x, y)
							//Now we verify it isn't covered by getting the max height we can be at for this tile
							if (!underGround)
							{
								//These first checks are for adjacent tiles (tiles immediately beside our tile and below them
								//If we are on an odd tile (front), don't check y2 = y first
								int modY = 0;
								int modX;
								if (x % 2 != 0)
									modY = 1;
								if (mousePos.x >= tX + 32.f && x < width - 1)
									modX = 1;
								else if (x > 0)
									modX = -1;
								else
									modX = 0;

								if (modX != 0)
								{
									for (int y2 = y + modY; y2 < height; y2++)
									{
										if (tiles[x + modX][y2].getPosition().z > 0.f)
										{
											//Now we check if the location is valid
											tX = tiles[x + modX][y2].getPosition().x;
											tY = tiles[x + modX][y2].getSpritePosition().y + 32.f;
											//If tiles at tX, tY is above tiles at x, y; that means not valid
											if (getMin(mousePos.x - tX) + tY + 32.f < mousePos.y)
												valid = false;
										}
									}
								}

								//This last check is for tiles in the same row as (x,y) to check height validation
								for (int y2 = y + 1; y2 < height; y2++)
								{
									if (tiles[x][y2].getPosition().z > 0.f)
									{
										//Now we check if the location is valid
										tX = tiles[x][y2].getPosition().x;
										tY = tiles[x][y2].getSpritePosition().y + 32.f;
										//If tiles at tX, tY is above tiles at x, y; that means not valid
										if (getMin(mousePos.x - tX) + tY < mousePos.y)
											valid = false;
									}
								}
							}

							if (valid) //If no invalidities, we select this tile
							{
								selectedTile = sf::Vector2i(x, y);
								return;
							}

						}
					}
				}
			}
		}
	}
}	

float Map::getMin(float x)
{
	if (x < 32.f)
	{
		return (-0.5f*x + 16.f);
	}
	else
	{
		return (0.5f*x - 16.f);
	}
}

float Map::getMax(float x)
{
	if (x < 32.f)
	{
		return (0.5f*x + 16.f);
	}
	else
	{
		return (-0.5f*x + 48.f);
	}
}

int Map::load(std::string name)
{
	std::ifstream map;
	map.open("../Maps/" + name + ".map");
	
	if (!PathIsDirectory(_T("../Maps")) || !map.good())
	{
		//File not loaded or directory non-existent
		return 1;
	}

	try
	{
		//Clear old data
		clear();
		map >> name
			>> width >> height;
		int camx, camy, grid, ground;
		map >> camx >> camy >> grid >> ground;
		cam.setPosition(sf::Vector2f((float)camx, (float)camy));
		cam.setSize(sf::Vector2f(1024.f,768.f));
		//Set gui state
		if (grid > 0)
			gridlines = true;
		else
			gridlines = false;
		if (ground > 0)
			underground = true;
		else
			underground = false;
		//Now create the array
		tiles = new Tile*[width];
		//And initialize each tile
		for (int i = 0; i < width; i++)
			tiles[i] = new Tile[height];

		//Although it is going to be slower...
		//Each newly loaded tile will be cross checked with pre-loaded data
		//If a discrepancy occurs, map file is corrupt.
		int in1, in2, in3; //Loading variables
		sf::Vector3f pos; //Temp vector for position loading
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				map >> in1;
				//Invalid ID detection
				if (TextureDictionary::getID(in1) == Textures::ID::Blank)
				{
					map.close();
					return 1; //Corrupt data file
				}
				tiles[x][y].setImageID(TextureDictionary::getID(in1));
				tiles[x][y].setCoords(x, y);
				map >> in1 >> in2 >> in3;
				pos = sf::Vector3f((float)in1, (float)in2, (float)in3);
				//Check previously loaded tiles for issues
				//(No two tiles can be in the same spot)
				for (int lx = 0; lx < x; lx++)
				{
					for (int ly = 0; ly < y; ly++)
					{
						if (tiles[lx][ly].getPosition() == pos) //Discrepancy detected
						{
							map.close();
							return 1; //Corrupt data file
						}
					}
				}
				tiles[x][y].setPosition(pos);
			}
		}
	}
	catch (...)
	{
		map.close();
		return -1; //Unhandled error is -1
	}

	map.close();
	return 0; //Success
}

int Map::save(std::string name)
{
	//First verify a map directry exists, if not, create one
	if (!PathIsDirectory(_T("../Maps")))
	{
		CreateDirectory(_T("../Maps"), NULL);
	}

	std::ofstream map;
	try
	{
		map.open("../Maps/" + name + ".map");
		int grid, ground;
		if (gridlines)
			grid = 1;
		else
			grid = 0;
		if (underground)
			ground = 1;
		else
			ground = 0;
		//Now we serialize the map data to file
		map << name << std::endl
			<< width << std::endl
			<< height << std::endl
			<< (int)cam.getPosition().x << std::endl
			<< (int)cam.getPosition().y << std::endl
			<< grid << std::endl
			<< ground << std::endl;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				if (map.good())
					tiles[x][y].save(map);
				else
				{
					map.close();
					return 1; //Error
				}
			}
		}
		map.close();
	}
	catch (...)
	{
		map.close();
		return -1; //Unhandled error is -1
	}
	return 0;
}

Map::~Map()
{
	clear();
}
