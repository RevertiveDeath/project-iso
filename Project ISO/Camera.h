#pragma once
#include "Map.h"
#include <SFML\Graphics.hpp>
#include "SFML/Graphics.hpp"

/* There can be multiple cameras (one for each window with a screen property).
 * This allows us to only draw the map once and just use cameras to display a portion of the view buffer.
*/

class Camera
{
private:
	//There are 3 zoom modes
	float scale; //0.5f is zoomed out, 1.f is standard size, 2.f is 2X zoom
	sf::Vector2f position; //Top left coordinate
	sf::Vector2f size; //In pixels
	sf::Vector2i tileSize; //In tiles

	void calcTileSize()
	{
		int modWidth = (int)size.x;
		while (modWidth % 64 != 0)
			modWidth--;
		if (modWidth == 0)
			modWidth = 1024;
		int modHeight = (int)size.y;
		while (modHeight % 32 != 0)
			modHeight--;
		if (modHeight == 0)
			modHeight = 768;

		int width = (modWidth / 64) + ((modWidth / 64) - 1);
		int height = (modHeight / 32);

		tileSize = sf::Vector2i(width, height);
	}

public:
	//Constructors
	Camera()
	{
		clear();
	}
	Camera(float newScale, sf::Vector2f newPosition, sf::Vector2f newSize)
	{
		scale = newScale;
		position = newPosition;
		size = newSize;
		calcTileSize();
	}

	//Methods
	void clear()
	{
		Camera(1.f, sf::Vector2f(0.f, 0.f), sf::Vector2f(0.f, 0.f));
	}

	//Getters
	float getScale()
	{
		return scale;
	}
	sf::Vector2f getPosition()
	{
		return position;
	}
	sf::Vector2f getSize()
	{
		return size;
	}
	sf::Vector2i getTileSize()
	{
		return tileSize;
	}
	//Setters
	void setScale(float newScale)
	{
		scale = newScale;
	}
	void setPosition(sf::Vector2f newPosition)
	{
		position = newPosition;
	}
	void setSize(sf::Vector2f newSize)
	{
		size = newSize;
		calcTileSize();
	}
};

//The CameraManager class manages the cameras for the game
//There is a main camera and then an array of window cameras assigned (via pointers) to window ids
//If a window is removed in all parent GUI windows, delete the camera
class CameraManager
{
private:
	Camera main; //Main map camera, viewport for the player
public:
	void resetMain()
	{
		//Resets main camera
		main.clear();
	}
	//Move main camera
	void move(sf::Vector2f dir)
	{
		main.setPosition(main.getPosition() + dir);
	}
	void move(float mX, float mY)
	{
		move(sf::Vector2f(mX, mY));
	}
	//Any Manager setters and getters refer to the main cam
	//This is because any windows that are open contain a reference to the canera
	//So the GUI has full access to any child cameras anyways
	//Getters
	float getScale()
	{
		return main.getScale();
	}
	sf::Vector2f getPosition()
	{
		return main.getPosition();
	}
	sf::Vector2f getSize()
	{
		return main.getSize();
	}
	sf::Vector2i getTileSize()
	{
		return main.getTileSize();
	}
	//Setters
	void setScale(float newScale)
	{
		main.setScale(newScale);
	}
	void setPosition(sf::Vector2f newPosition)
	{
		main.setPosition(newPosition);
	}
	void setSize(sf::Vector2f newSize)
	{
		main.setSize(newSize);
	}
};