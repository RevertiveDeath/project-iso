ISO_SCRIPT:

Contents:
X. INTRO
  X.1 FOREWARD
1. WRITING A SCRIPT
  1.1 STARTING AND ENDING A SCRIPT
  1.2 CREATING A SIMPLE EVENT
  1.3 CHAINING SCRIPTS
2. COMMANDS/SYNTAX
  2.1 CONTROL
  2.2 EVENT
  2.3 ACTION
  2.4 CONDITION

---------------------------------------------------------------------------------------------------------------------------
[x] INTRO


X.1 FOREWARD

The purpose of ISO_SCRIPT is to provide a very basic form of scripting to Project ISO's engine. Since the scripts and
tokens are very closely related (And structured towards) the games inner mechanics, this custom scripting language will
make it much easier for me (The developer) to hand craft the game experience. Although the creation of this language is
not meant to invite others to script their own custom stories into the game, it is entirely doable. This is the purpose
behind creating a manual.

---------------------------------------------------------------------------------------------------------------------------
[1] WRITING A SCRIPT

1.1 STARTING AND ENDING A SCRIPT

To start writing a script use the keyword: START
Then, to allow events to occur, follow it with the number of events: START EVENTCOUNT #

Here you replace the # with the number of events to follow.

At the end of the script simply write END. 




1.2 CREATING A SIMPLE EVENT
*This is an example script on how to move a camera from coords (x1,y1) to (x2,y2)

--------------------------------
START EVENTCOUNT 1

EVENT PRIORITY 1
  CONDITIONCOUNT 1
    CONDITION CAMERA_AT x1 y1
  ACTIONCOUNT 1
    ACTION MOVE_CAMERA x2 y2

END
-------------------------------

As you can see, it is good practice to list all the conditions and action amounts beforehand to allocate memory correctly.
However, if you list a count at 1 and supply 2 events, the second will be skipped.

A complete list of conditions and actions will be supplied at the end of this document.

*Make sure you complete the script in the right order: START




1.3 CHAINING SCRIPTS

Script can reference the location of subsequent scripts to load at the end.
To do this simply write CHAIN and afterwards specify the path from this script's location, to the chained script.

For example, if you wanted to create a game start script that simply runs a bunch of child scripts:

*In this example, this script is located at "C:\Project ISO\Scripts\ExampleFolder\start.isf"
--------------------------------
START EVENTCOUNT 0

CHAIN "..\ExampleFolder2\script.isf"
CHAIN "..\ExampleFolder2\script2.isf"
CHAIN "end.isf "

END
-------------------------------

The first two scripts would be located at "C:\Project ISO\Scripts\ExampleFolder2".
To backtrack to a parent folder, use the ".." operator.
To access a script in the current script's folder. Simply write the scripts name: end.isf

*If a child script adds another script, it will be inserted directly after the currently executing script
*If ..\ExampleFolder2\script.isf added another script named script3.isf, it would execute before script2.isf

---------------------------------------------------------------------------------------------------------------------------
[2] COMMANDS/SYNTAX

2.1 CONTROL

CHAIN: followed by a relative path to a script file to chain scripts together
ex: CHAIN "script2.isf"

START: indicates start of script file, followed by EVENTCOUNT token
ex: START EVENTCOUNT 4

END: indicates end of script file

EVENTCOUNT: followed by the number of events in the script, this follows a START token




2.2 EVENT

ACTIONCOUNT: indicates the number of actions this event will execute

CONDITIONCOUNT: indicates the number of conditions required before the actions can occur

EVENT: indicates start of an event, followed by PRIORITY token
ex: EVENT PRIORITY 10

PRIORITY: guides the order of event execution (Events of higher priority execute first)




2.3 ACTION

ACTION: indicates start of an action, followed by the action type (token)
ex: ACTION MOVE_CAMERA 100 200

(The following is a list of valid action types and acceptable parameters)
*Square brackets [] indicate a parameter, do not include the brackets in scripts!

MOVE_CAMERA [x] [y]: moves the main camera from current coords to x,y instantly
LOAD_MAP [name]: loads map with indicated name, if it exists
SAVE_MAP [name]: saves map with indicated name, overwrites if it exists
CREATE_MAP [x] [y] [name]: creates a map with pixel width x, pixel height y and saves it to file with given name

2.4 CONDITION

CONDITION: indicates start of a condition, followed by the condition type (token)
**NOT YET IMPLEMENTED